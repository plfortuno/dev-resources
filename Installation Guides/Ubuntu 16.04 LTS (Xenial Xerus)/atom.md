## Prerequisites:
git [fix internal links when committed to master]

## Official Instructions:
[https://flight-manual.atom.io/getting-started/sections/installing-atom/](https://flight-manual.atom.io/getting-started/sections/installing-atom/)

## Instructions:

Download the .deb [here](https://atom.io/)

Move the terminal to where the directory of the package
```bash
sudo dpkg -i atom-amd64.deb
sudo apt-get -f install
```
From here you can use
```bash
atom
```
to launch or you can just pin it to taskbar
