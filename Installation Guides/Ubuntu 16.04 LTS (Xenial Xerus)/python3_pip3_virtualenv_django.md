This is for python3, some other people in CSI use python2 so if you use that try asking around

Okay so a lot of times online you'll see commands for 'python', if it doesn't work for you use 'python3'

Usually python3 is already installed, if not:
```bash
sudo apt-get update
sudo apt-get install python3
```
## Instructions:
```bash
sudo apt-get install python3-pip
sudo pip3 install virtualenv
sudo pip3 install Django==1.11.5
```