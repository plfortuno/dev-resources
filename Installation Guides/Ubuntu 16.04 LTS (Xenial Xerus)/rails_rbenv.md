## Prerequisites:
git
nodejs
package_soup
[FIX THESE INTERNAL LINKS ONCE COMMITTED TO MASTER]

## Previous guide (this guide mostly takes from Neil's):
https://gitlab.com/up-csi/dev-resources/blob/master/ruby/learn_ruby_on_rails.md

## Official Instructions for rbenv:
https://github.com/rbenv/rbenv

## Instructions:
*IF YOU HAVE RVM YOU SHOULD PURGE IT FIRST SINCE IT'S INCOMPATIBLE WITH RBENV*

Clone rbenv to ~.rbenv
```bash
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
```

Change $PATH so it can access rbenv command-line
check whether it's .bash_profile or .bashrc by doing these commands
```bash
cd ~
ls -a
```
For me, it was ~/.bashrc. 

```bash
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
OR
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
```
Do the instructions given out by this command:
$ ~/.rbenv/bin/rbenv init

For me, it gave this: 
```bash
# Load rbenv automatically by appending  
# the following to ~/.bashrc:  

eval "$(rbenv init -)"
```
So I opened ~/.bashrc with a text editor and did that -  
Actually no that's boring I did this:
```bash
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
```
Restart the terminal shell (so PATH takes effect)

Verify installation with this command:
```bash
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
```
You should see something like this:
```bash
Checking for `rbenv' in PATH: /home/third/.rbenv/bin/rbenv
Checking for rbenv shims in PATH: OK
Checking `rbenv install' support: bash: line 114: : command not found
 ()
Counting installed Ruby versions: none
  There aren't any Ruby versions installed under `/home/third/.rbenv/versions'.
  You can install Ruby versions like so: rbenv install 2.2.4
Checking RubyGems settings: OK
Auditing installed plugins: OK
```

So if you're using rbenv for more or less the same way I'm gonna use it (for rails), you should install the ruby-build plugin
```bash
mkdir -p "$(rbenv root)"/plugins
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
```
To install a ruby version
```bash
rbenv install 2.4.0
```
(replace 2.4.0 with the ruby version you want)
```bash
gem install rails
gem install bundler
```
Aaaand you're set! :D

Updating rbenv:
```bash
cd ~/.rbenv
git pull
```