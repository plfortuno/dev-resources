# OS: Ubuntu 16.04 LTS

I document each software related to devwork in this directory. Other operating systems might have different instructions.

Each tool usually has its own file.

Commands meant to be entered through terminal look like this:
```bash
command here
```

Just message me (or anyone in charge of this) if there's discrepancies or errors here.

\- Pio
