## Prerequisites:
npm

## Official Instructions:
[https://vuejs.org/v2/guide/installation.html](https://vuejs.org/v2/guide/installation.html)

## Instructions:
```bash
npm install vue
```