# JavaScript

## Introduction

Javascript is a very ubiquitous language in the field of web development, especially in giving life to webpages and web apps. Recent development of tools and NodeJS allow Javascript to be used even in making server-side apps, native apps, and even mobile apps.

## Useful Links and Tutorials

### Quick Start

[W3 Javascript tutorial](https://www.w3schools.com/js/)
[CodeAcademy Javascript tutorial](https://www.codecademy.com/learn/introduction-to-javascript)

### Cheat Sheets

[<MyFavorite/> Javascript Cheat Sheet](https://www.onblastblog.com/javascript-cheat-sheet/)
[<MyFavorite/> HTML Cheat Sheet](https://www.onblastblog.com/javascript-cheat-sheet/)
[Codementor Javascript Cheat Sheet](https://www.codementor.io/johnnyb/javascript-cheatsheet-fb54lz08k)
[Another Cheat Sheet](https://goo.gl/o1fyvX)
[Cheatography Javascript Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/javascript/)

### NodeJS (introduction to advanced JS)

[Node.js tutorial](https://nodeschool.io/)
After learning the basics, you can skip the _javascripting_ tutorial, and proceed with _learnyounode_.

>Credits: Adel Mandanas
