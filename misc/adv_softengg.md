# Let's level up for excellent software engineering!

## Web/Client-Server Architecture
0. Web Application [architecture](https://webingineer.wordpress.com/2010/07/24/web-application-architechture-client-server-architecture/)
1. Client-Server [architecture](https://spin.atomicobject.com/2015/04/06/web-app-client-side-server-side/)

## Agile and Scrum Methodology

0. Official [website](http://agilemethodology.org/) of agile methodology.
1. Agile [manifesto](http://agilemanifesto.org/)
2. Twelve [principles](http://agilemanifesto.org/principles.html) of agile software
3. Agile in a [nutshell](http://www.agilenutshell.com/)
4. Scrum [methodology](http://scrummethodology.com/)
5. UPCSI Scrum/Agile [keynotes](https://drive.google.com/file/d/0Bw071lDOsdjINm5jeDJLX0w0VEk/view?usp=sharing)
6. UPCSI Project [Book](https://drive.google.com/file/d/0Bw071lDOsdjIOThaMzZPWUNrWVU/view?usp=sharing)
7. Creating user stories [1](https://www.mountaingoatsoftware.com/agile/user-stories), [2](http://www.agilemodeling.com/artifacts/userStory.htm), [3](http://www.romanpichler.com/blog/10-tips-writing-good-user-stories/), [4](https://help.rallydev.com/writing-great-user-story)

## What is TDD?

Pardon to use [Wikipedia](https://en.wikipedia.org/wiki/Test-driven_development), but this is always a good start.

0. Philosophy: Maker-Checker [Principle](http://lorenzo-dee.blogspot.com/2014/11/maker-checker-design-concept.html)
1. A Guide to Testing [Rails](http://guides.rubyonrails.org/testing.html) Applications
2. Web Testing: A Complete [guide](http://www.softwaretestinghelp.com/web-application-testing/) about testing web applications
3. Testing Your Web [Apps](https://www.adminitrack.com/articles/TestingYourWebApps.aspx)

## Automated Testing

**Ruby**

0. [Rails](http://guides.rubyonrails.org/testing.html)
1. [RSpec](http://rspec.info/)
2. [Test::Unit](http://learnrubythehardway.org/book/ex47.html)
3. Ruby [Toolbox](https://www.ruby-toolbox.com/)
4. CodeSchool: Rails Testing [for Zombies](https://www.codeschool.com/courses/rails-testing-for-zombies)

**Python**

0. [Django](https://docs.djangoproject.com/en/1.9/topics/testing/)
1. [Flask](http://flask.pocoo.org/docs/0.11/testing/)
2. [Selenium](http://selenium-python.readthedocs.io/)
3. Web [scraping](http://www.marinamele.com/selenium-tutorial-web-scraping-with-selenium-and-python) with Python and Selenium
4. [unittest](https://docs.python.org/2.7/library/unittest.html)

**Sample Django Project (TDD)**:
[Scrapbook](https://github.com/formidablefrank/scrapbook)

**JavaScript**
0. [Mocha](http://mochajs.org)
1. [Chai](http://chaijs.com)
2. [Sinon.JS](http://sinonjs.org)
4. [QUnit](http://qunitjs.com)
5. Ask [Google](https://www.google.com/search?q=javascript+testing+framework&ie=utf-8&oe=utf-8#)

**Some insights about TDD**:
[CS 260 Final Paper](https://drive.google.com/file/d/0B9147eOtQN5xUlhIemdqdlpQVFE/view?usp=sharing)

## Risk Management

0. [Software Risk Management, A Practical Guide](http://www.asq509.org/ht/a/GetDocumentAction/i/110477)

## Software Quality Assurance

0. [Clean Code](http://ricardogeek.com/docs/clean_code.pdf)
1. Clean Code [Cheat Sheet](http://www.planetgeek.ch/wp-content/uploads/2013/06/Clean-Code-V2.1.pdf)
2. The Clean Code [Blog](http://blog.cleancoder.com/uncle-bob/2016/03/19/GivingUpOnTDD.html)
3. [The Clean Coder](http://ptgmedia.pearsoncmg.com/images/9780137081073/samplepages/0137081073.pdf)
4. 9 [things](http://blog.christoffer.me/9-things-i-learned-from-reading-the-clean-coder-by-robert-c-martin-on-how-professional-developers-conduct-themselves/) from the 'Clean Coder'
5. [Source Making](https://sourcemaking.com/)

## Advanced VCS Techniques

0. [git ready!](http://gitready.com/)
1. [CodeSchool: Git Real 2](https://www.codeschool.com/courses/git-real-2)
2. [Github: Advanced Git](https://help.github.com/categories/advanced-git/)
3. Learn [Git branching](http://learngitbranching.js.org/)

## Sprint Metrics

0. [Atlassian: Five agile metrics you won't hate](https://www.atlassian.com/agile/metrics)

## Continuous Integration
0. [Gitlab CI](https://about.gitlab.com/gitlab-ci/)
