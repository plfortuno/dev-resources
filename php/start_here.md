# PHP

## Introduction

PHP is a server-side language that is widely-used in the industry. The demand is high, and thus learning this language and the tools that come with and use it is a good investment.

## Installation

* How to install [XAMPP](http://www.wikihow.com/Install-XAMPP-on-Linux)
> Note: Installing XAMPP in Windows may be easier since there is a dedicated .exe file to installing it.

## Useful Links and Tutorials

* [PHP cheat sheet](https://blueblots.com/development/php-cheat-sheets/)

# Wordpress

## Introduction

PHP is most often used in Wordpress, which is very common in making company websites and blogs, or anywhere where there is a need to manage content like posts and images.

## Installation

* Install WordPress on [XAMPP](https://premium.wpmudev.org/blog/setting-up-xampp/)
* Install WordPress on [LAMP](https://www.tecmint.com/install-wordpress-on-ubuntu-16-04-with-lamp/)

## Useful Links and Tutorials

* Using [WordPress](https://www.tutorialspoint.com/wordpress/)
* Custom theme on [WordPress](https://codex.wordpress.org/Theme_Development)

>Credits: Frank Rayo
