# Python Resources

## Beginner Tutorials and Cheatsheets

* [Youtube Tutorial](https://www.youtube.com/watch?v=N4mEzFDjqtA)
* [Python3 in One Pic](https://raw.githubusercontent.com/coodict/python3-in-one-pic/master/py3%20in%20one%20pic.png)
  * Just one huge PNG image summarizing all of Python3's syntax
* [Python Notes PDF](https://drive.google.com/file/d/0B30BxjRT6yhdWEVMSXQwaExYSDA/view)
  * A more detailed cheatsheet

## Notes

* We recommend [Sublime Text](https://www.sublimetext.com/), a text editor that runs fast and has many nice features.
  * We also recommend [Atom](https://atom.io), as it also has many nice features and is open source, but it's somewhat slower to start.
* If you have no experience on HTML and CSS, read up on them, and try to make a really simple webpage. No need to delve into it, just learn the basics.

>Credits: Carl Araya
