# UP CSI Development Resources

This repository serves as the compilation of every development **guides**, **tutorials**, **tips**, and **conventions** of [UP Center for Student Innovations](http://www.facebook.com/upcsi).

## Getting Started
* [FAQ](https://gitlab.com/up-csi/dev-resources/blob/master/faq.md)
* [Recommended Development Tools](https://gitlab.com/up-csi/dev-resources/blob/master/misc/development_tools.md)
* [Installing Linux with another OS](https://gitlab.com/up-csi/dev-resources/blob/master/misc/linux_installation.md)

## Guides and Tutorials

### General Web Dev - HTML/CSS/JS/WebFrameworks
* [CodeAcademy](https://www.codecademy.com/)
* [CodeSchool](https://www.codeschool.com/)
* [KhanAcademy](https://www.khanacademy.org/computing/computer-programming)
* [Enki: Learn better code, daily](https://play.google.com/store/apps/details?id=com.enki.insights) - Android app

### Ruby
* [Install and Learn Ruby on Rails](https://gitlab.com/up-csi/dev-resources/blob/master/ruby/learn_ruby_on_rails.md)
* [Installing pry for Rails 4](https://gitlab.com/up-csi/dev-resources/blob/master/ruby/pry_for_rails_linux.md)

### Python
* [**Start here**](https://gitlab.com/up-csi/dev-resources/blob/master/python/start_here.md)
* [Installing Django and Development Tips](https://gitlab.com/up-csi/dev-resources/blob/master/python/django_install_instructions.md)
* [Learn Mezzanine](https://gitlab.com/up-csi/dev-resources/blob/master/python/learn_mezzanine.md)

### PHP
* [**Start here**](https://gitlab.com/up-csi/dev-resources/blob/master/php/start_here.md)
* [Building your own LNPP Stack](https://gitlab.com/up-csi/dev-resources/blob/master/php/build_lnpp_stack.md)
* [Laravel](https://laravel.com/docs/5.4): PHP framework for Web Artisans

### JavaScript
* [**Start here**](https://gitlab.com/up-csi/dev-resources/blob/master/js/start_here.md)
* Install NodeJS using [NVM](https://github.com/creationix/nvm)
* Introduction to Angular with Mr. [Johnny](https://speakerdeck.com/johnnyestilles/intro-to-angularjs-1-dot-x) Estilles, Senior Software Engineer from Freelancer.ph
* Node, Grunt, Bower and Yeoman, a modern web dev's [toolkit](http://juristr.com/blog/2014/08/node-grunt-yeoman-bower/#Grunt)
* [Angular](https://angular.io) 2.0. It is recommended to read the tutorial for TypeScript. Need yeoman [generator](https://github.com/angular/angular-cli)?
* [Setting up MEAN](https://gitlab.com/up-csi/dev-resources/blob/master/js/setting_up_mean.md)
* How it feels to learn JavaScript in [2016](https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f#.4835wjspy)
* [Javascript Cheat Sheet](https://www.onblastblog.com/javascript-cheat-sheet/)

### HTML and CSS
* [SemanticUI](http://semantic-ui.com/)
* [Bootstrap](http://getbootstrap.com)
* MaterialCSS [1](http://materializecss.com/), [2](http://www.material-ui.com/), [3](https://www.muicss.com/)
* [AdminLTE](https://github.com/almasaeed2010/AdminLTE)
* An ultimate guide to [flat](http://www.webdesignerdepot.com/2013/09/the-ultimate-guide-to-flat-design/) Design
* An in-depth look on flat [design](http://www.awwwards.com/flat-design-an-in-depth-look.html)
* [HTML5 Cheat Sheet](https://www.onblastblog.com/html5-cheat-sheet/)

### Mobile
* [Ionic Framework](http://ionicframework.com)
* [Android Development Environment](https://gitlab.com/up-csi/dev-resources/blob/master/mobile/android_dev_env.md)
* [Ionic or Android?](http://wijmo.com/blog/mobile-app-development-how-to-decide-on-hybrid-vs-native/)
* [Hybrid vs Native Mobile Apps](http://www.gajotres.net/hybrid-vs-native-apps/)
* [Why did I choose the Ionic Framework for Mobile App Development?](http://gonehybrid.com/why-did-i-choose-the-ionic-framework-for-mobile-app-development/)

### DevOps
* [Advanced Software Engg Practices](https://gitlab.com/up-csi/dev-resources/blob/master/misc/adv_softengg.md)
* [Special Interest Groups' Resources](https://gitlab.com/up-csi/dev-resources/blob/master/misc/special_groups.md)

## How to be a CS lord??
Learn more [here](https://docs.google.com/presentation/d/1fswCdcCDN20AjCXNnBsIH5CVRmSZ0EcRmoQ4zlwL5xA/edit?usp=sharing).

## Installation Scripts
* [Ruby on Rails Installation Script](https://gitlab.com/up-csi/dev-resources/raw/master/ruby/install-ror.sh)
* [Ruby on Rails DBs Installation Script](https://gitlab.com/up-csi/dev-resources/raw/master/ruby/install-ror-db.sh)
* [Django Installation Script](https://gitlab.com/up-csi/dev-resources/raw/master/python/django_install.sh)

## Other Useful Acad Resources

### Useful Links
1. The Daily [WTF](http://thedailywtf.com/): Curious Perversions in Information Technology
2. [ZealDocs](http://zealdocs.org): Offline docs browser
3. [DevDocs](http://devdocs.io): Web-based offline docs browser
4. [Cheat](https://github.com/chrisallenlane/cheat): Interactive terminal cheatsheet

### IT E-Books
Free download [here](http://it-ebooks.info/)!

### Data Structures and Algorithms Visualizer
* [Visualizer1](http://algo-visualizer.jasonpark.me/)
* [Visualizer2](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)
* [Visualizer3](http://visualgo.net/)
